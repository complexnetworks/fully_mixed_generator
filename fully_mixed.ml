(* ocamlopt -o fully_mixed str.cmxa unix.cmxa fully_mixed.ml *)

(* 
Generate a fully mixed link stream, with a fixed number of nodes, fixed duration, fixed and homogeneous activity rate. 
Principle: 
- compute probability for a link to appear at each time step
- for each pair, draw to see if the link exist
Warning: complexity in O(num_nodes**2.duration), clearly not optimal here / undirected flow
*)

(* ***** arguments ***** *)

let in_num_nodes = Sys.argv.(1);; (* int, number of nodes in the link stream *)
let in_duration = Sys.argv.(2);; (* int, number of timestamps *)
let in_activity = Sys.argv.(3);; (* int, average number of occurrences of a link in the whole stream *)


(* ***** general tools ***** *)

Random.self_init ();;
let ios x = int_of_string x;;
let fos x = float_of_string x;;
let foi x = float_of_int x;;


(* ***** generation ***** *)

let generating_stream num_nodes duration activity =
  let proba_occur = activity /. (foi duration) in
  for t=1 to duration
  do
    for node_1=0 to num_nodes-2 
    do
      for node_2=node_1+1 to num_nodes-1
      do
	let ran = (foi (Random.int 1000000)) /. 1000000. in
	if ran <= proba_occur
	then
	  begin
	    print_int t;
	    print_string " ";
	    print_int node_1;	    
	    print_string " ";
	    print_int node_2;
	    print_string "\n";
	  end
      done;      
    done;
  done;;


(* ***** main ***** *)

generating_stream (ios in_num_nodes) (ios in_duration) (fos in_activity);;
