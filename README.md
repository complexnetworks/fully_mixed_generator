# README fully_mixed.ml #

Generate a fully mixed link stream, with a fixed number of nodes, fixed duration, fixed and homogeneous activity rate. 
Principle: 

- compute probability for a link to appear at each time step

- for each pair, draw to see if the link exist

Warnings: The complexity is in O(num_nodes**2.duration), clearly not optimal here. The stream is undirected.


### Compilation ###

To compile, you need an Ocaml compiler ocamlc or ocamlopt, see
http://ocaml.org/

To generate the executable file fully_mixed from the source file fully_mixed.ml , type:
```
ocamlopt -o fully_mixed str.cmxa unix.cmxa fully_mixed.ml
```
or
```
ocamlc -o fully_mixed str.cma unix.cma fully_mixed.ml
```

### Usage, inputs, output ###

```
./fully_mixed [number_nodes] [duration] [activity]
```

number_nodes denotes the number of nodes in the stream

duration the number of timestamps in the stream

activity is the average number of occurrences of any link in the stream

The program prints in the standard output an undirected flow in the format:
```
timestamp id1 id2 
...
```

### Examples ###
  
```
./fully_mixed 50 100 5
```